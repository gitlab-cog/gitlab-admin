[![coverage report](https://gitlab.com/gitlab-cog/gitlab/badges/master/coverage.svg)](https://gitlab.com/gitlab-cog/gitlab/commits/master)

# No Further Cog Bundle Development

Since cog is [no longer maintained](https://github.com/operable/cog), we will be 
deprecating it over time and instead developing [GitLab ChatOps](https://gitlab.com/gitlab-org/gitlab-ce/issues/34311).
For now, this means that no new features should be added to this repo (or the 
other gitlab-cog group repos). Existing features can continue to be used until 
the new solution is in place. Contributions to GitLab ChatOps are welcomed.

# GitLab Bundle for Cog

This bundle provides commands for interactions with GitLab.
Permissions are used to control which users are authorized to run each command.

To view more information about the bundle and the documentation for each
command use Cog's help command:

For bundle information:

```
help gitlab
```

For specific command information:

```
help gitlab:broadcast
```

## Testing locally

Run the command like this
```
COG_COMMAND="user-find" COG_ARGV_0=<first arg> COG_ARGC=1 GITLAB_TOKEN=<token> ./cog-command
```

## Configuration

* GITLAB_URL - Required URL of your GitLab instance
* GITLAB_TOKEN - Required admin access token used to authenticate with GitLab API

You can set these environment variables with Cog's dynamic config feature:

```
echo 'GITLAB_URL: "https://gitlab.com"' >> config.yaml
echo 'GITLAB_TOKEN: "AKJAJ4OZ5KYFVRVKZRWM"' >> config.yaml
cogctl bundle config create gitlab config.yaml --layer=base
```

When creating the access token for use with the following environment variables,
make sure the GitLab user that owns the token has admin rights.

## Development

### Cut a new release (build, push, and tag on GitLab):

This bundle gets pushed to dockerhub automagically by CI when a branch is merged to master. Just be sure of bumping up the version

### Deploy to Marvin

* SSH into `cog.gitlap.com` and sudo up
  * Inside root's home directory, cd into cog-util
  * Execute `update-bundle gitlab`
* In Slack, either in a private conversation with Marvin or in Infrastructure
  * `!bundle versions gitlab` to see the installed versions and the current running.
  * `!bundle disable gitlab` to disable the current version from service.
  * `!bundle enable gitlab 0.0.9` gitlab should be follwed by the version number that you tagged your release with.
