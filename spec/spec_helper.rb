require "simplecov"
SimpleCov.start do
  add_filter "vendor"
  add_filter "spec"
end


require "rspec"
require "webmock/rspec"
WebMock.disable_net_connect!(allow_localhost: true)

ENV['GITLAB_URL'] = "http://localhost"

def with_environment(*args)
  ENV["COG_ARGC"] = args.length.times { |n| ENV["COG_ARGV_#{n}"] = args[n] }.to_s
  yield
ensure
  ENV.delete("COG_ARGC")
  args.length.times { |n| ENV.delete("COG_ARGV_#{n}") }
end
