require 'spec_helper'
require 'cog_cmd/gitlab/user/find'
require 'cog_cmd/gitlab/user/block'
require 'cog_cmd/gitlab/user/unblock'
require 'cog_cmd/gitlab/user/email/update'

describe "find user command" do
  # This is so because gitlab-ci is not a tty and cog tries to read from it failing
  before { allow(STDIN).to receive(:tty?) { true } }

  it "fails with an error without a username" do
    finder = CogCmd::Gitlab::User::Find.new
    expect{ finder.run_command }.to raise_error /Username or email is required to perform a search/
  end

  it "Works to not find a user" do
    req = stub_request(:get, /api\/v4\/users\?username=.*/).
      with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
      to_return( status: 404 )

    with_environment("myuser") {
      finder = CogCmd::Gitlab::User::Find.new
      expect { finder.run_command }.to raise_error(/User myuser does not exists/)
    }

    remove_request_stub(req)
  end

  context "with a valid user" do
    let(:user) {
      {
        id: 1,
        username: "myuser",
	email: "myuser@myuser.com"
      }
    }

    it "can find a valid user" do
      req = stub_request(:get, /api\/v4\/users\?username=.*/).
        with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
        to_return lambda { |request| { status: 200, body: [user].to_json } }

      with_environment("myuser") {
        finder = CogCmd::Gitlab::User::Find.new
        finder.run_command
        expect(finder.response.content.size).to eq(1)
      }

      remove_request_stub(req)
    end

    it "can find a valid user by email" do
      req = stub_request(:get, /api\/v4\/users\?search=.*/).
        with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
        to_return lambda { |request| { status: 200, body: [user].to_json } }

      with_environment("myuser@myuser.com") {
        finder = CogCmd::Gitlab::User::Find.new
        finder.run_command
        expect(finder.response.content.size).to eq(1)
      }

      remove_request_stub(req)
    end


    it "can block it" do
      sentinel = {state: user[:state]}
      reqs = []
      reqs << (stub_request(:get, /api\/v4\/users\?username=.*/).
        with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
        to_return lambda { |request| { status: 200, body: [user].to_json } })
      reqs << (stub_request(:post, /api\/v4\/users\/1\/block/).
              with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
              to_return lambda { |request|
                sentinel[:state] = "blocked";
                { status: 201 }
              })
      with_environment("myuser") {
        finder = CogCmd::Gitlab::User::Block.new
        finder.run_command
        expect(finder.response.content).to eq("User myuser has been blocked")
      }
      reqs.each { |req| remove_request_stub(req) }
    end

    it "can unblock it" do
      sentinel = {state: "blocked"}
      reqs = []
      reqs << (stub_request(:get, /api\/v4\/users\?username=.*/).
        with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
        to_return lambda { |request| { status: 200, body: [user].to_json } })
      reqs << (stub_request(:post, /api\/v4\/users\/1\/unblock/).
              with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
              to_return lambda { |request|
                sentinel[:state] = user[:state];
                { status: 201 }
              })
      with_environment("myuser") {
        finder = CogCmd::Gitlab::User::Unblock.new
        finder.run_command
        expect(finder.response.content).to eq("User myuser has been unblocked")
      }
      reqs.each { |req| remove_request_stub(req) }
    end

    it "can update the email" do
      reqs = [
        (stub_request(:get, /api\/v4\/users\?username=.*/).
         with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
         to_return lambda { |request| { status: 200, body: [user].to_json } }),
      (stub_request(:put, /api\/v4\/users\/1\?email=.*/).
        with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
        to_return lambda {|request| { status: 200, body: [user].to_json } }
      )]

      with_environment("myuser", "new@email.com") {
        finder = CogCmd::Gitlab::User::Email::Update.new
        finder.run_command
        expect(finder.response.content).to eq("User myuser was sent a confirmation email to new@email.com")
      }

      reqs.each { |req| remove_request_stub(req) }
    end
  end
end
