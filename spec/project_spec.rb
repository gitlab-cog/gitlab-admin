require "spec_helper"
require "api/project"

module GitLab
  module API
    describe ProjectClient do
      let(:project) {
        {
          id: 1,
          public: true,
          default_branch: "master",
          name: "test project",
          name_with_namespace: "mygroup/test project",
          path: "myproject",
          path_with_namespace: "mygroup/somewhere",
          web_url: "http://url",
          created_at: "today",
          last_activity_at: "now",
          archived: false,
          star_count: 0,
          forks_count: 0,
					container_registry_enabled: true,
          repository_storage: "default",
					issues_enabled: true,
					jobs_enabled: true,
					lfs_enabled: true,
					merge_requests_enabled: true,
					request_access_enabled: true,
					shared_runners_enabled: true,
					snippets_enabled: true,
					wiki_enabled: true,
					public_jobs: true,
          description: "bla"
        }
      }
      it "returns a valid project when it exists" do
        req = stub_request(:get, /api\/v4\/projects\/.*/).
          with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
          to_return lambda {|request| { status: 200, body: project.to_json } }

        found_project = subject.find("mygroup/myproject")
        expect(found_project.to_h).to eq(project)

        remove_request_stub(req)
      end

      it "returns a null project when it doesn't exists" do
        req = stub_request(:get, /api\/v4\/projects\/.*/).
          with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
          to_return lambda {|request| { status: 404 } }

        expect{ subject.find("mygroup/myproject") }.to raise_error(
          /Could not find project with path mygroup\/myproject/)

        remove_request_stub(req)
      end

      it "returns a null project when the content is empty" do
        req = stub_request(:get, /api\/v4\/projects\/.*/).
          with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
          to_return lambda {|request| { status: 200, body: " " } }

        expect{ subject.find("mygroup/myproject") }.to raise_error(
          /Could not find project with path mygroup\/myproject/)

        remove_request_stub(req)
      end

      it "fails with an error when no project is provided" do
        expect{subject.find("")}.to raise_error(/A project path is required/)
      end

      it "fails with an error when a nil project is provided" do
        expect{subject.find(nil)}.to raise_error(/A project path is required/)
      end

      it "fails with an error when the server errs out" do
        req = stub_request(:get, /api\/v4\/projects\/.*/).
          with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
          to_return lambda {|request| { status: 500 } }

        expect{subject.find("something/something")}.to raise_error(
          /Failed to get project something\/something: 500/)

        remove_request_stub(req)
      end

    end
  end
end
