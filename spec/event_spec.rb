require "spec_helper"
require "api/user"

module GitLab
  module API
    describe Events do
      let(:user) {
        {
          id: 1,
          username: "myuser",
          name: "A guy",
          email: "the@email.com",
          state: "active",
          created_at: "yesterday",
          two_factor_enabled: "false",
          current_sign_in_at: "just a minute ago",
          last_sign_in_at: "this morning"
        }
      }

      let(:event){
        {
          created_at: "yesterday",
          action_name: "created",
          target_type: "note",
          target_title: "Some Content"
        }
      }

      it "can get a user most recent event" do
        reqs = [
          (stub_request(:get, /api\/v4\/users\?username=.*/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return lambda { |request| { status: 200, body: [user].to_json } }),
          (stub_request(:get, /api\/v4\/users\/1\/events\?per_page=.*/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return lambda {|request| { status: 200, body: [event].to_json } })]

        expect(subject.last("myuser").to_h).to eq(event)

        reqs.each { |req| remove_request_stub(req) }
      end

      it "gets an empty event when the content is empty" do
        reqs = [
          (stub_request(:get, /api\/v4\/users\?username=.*/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return( status: 200, body: [user].to_json )),
          (stub_request(:get, /api\/v4\/users\/1\/events\?per_page=.*/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return( status: 200, body: [].to_json ))
        ]

        expect(subject.last("myuser").to_a).to be_empty
        reqs.each { |req| remove_request_stub(req) }
      end

      it "fails with an error when the server errs out on getting event" do
        reqs = [
          (stub_request(:get, /api\/v4\/users\?username=.*/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return( status: 200, body: [user].to_json )),
          (stub_request(:get, /api\/v4\/users\/1\/events\?per_page=.*/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return( status: 500 ))
        ]

        expect{subject.last("myuser")}.to raise_error(
          /Failed to get Events for user myuser: 500/
        )

        reqs.each { |req| remove_request_stub(req) }
      end

      it "gets an empty event when it doesn't exists" do
        reqs = [
          (stub_request(:get, /api\/v4\/users\?username=.*/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return( status: 200, body: [user].to_json )),
          (stub_request(:get, /api\/v4\/users\/1\/events\?per_page=.*/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return( status: 404 ))
        ]

        expect(subject.last("myuser")).to be_nil

        reqs.each { |req| remove_request_stub(req) }
      end

        it "fails with an error when no username is provided to get event for" do
        expect{subject.last("")}.to raise_error(/A username is required/)
      end

      it "fails with an error when a nil username is provided to get event for" do
        expect{subject.last(nil)}.to raise_error(/A username is required/)
      end
    end
  end
end
