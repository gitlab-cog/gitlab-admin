require "spec_helper"
require "api/client"

module GitLab
  module API
    describe Client do
      context "by default" do
        before { ENV['GITLAB_URL'] = "https://gitlab.com" }

        it "points to gitlab.com" do
          expect(subject.url).to eq("https://gitlab.com")
        end
        it "has no api token" do
          expect(subject.token).to eq("no-token")
        end

        after { ENV['GITLAB_URL'] = "http://localhost" }
      end

      context "using a stubbed server" do
        before do
          stub_request(:get, /test/).
            with(headers: {'PRIVATE-TOKEN' => 'no-token'}).
            to_return(status: 200, body: "A body")

          stub_request(:get, /test-not-found/).
            to_return(status: 404)

          stub_request(:post, /test/).
            to_return lambda { |request| {status: 200, body: request.body } }

          stub_request(:put, /test/).
            to_return lambda { |request| {status: 200, body: request.body } }
        end

        it "gets data" do
          expect(subject.get("/test").body).to eq("A body")
        end

        it "works on 404" do
          expect(subject.get("/test-not-found").code).to eq("404")
        end

        it "posts data" do
          expect(subject.post("/test", key: "value").body).to eq("key=value")
        end

        it "puts data" do
          expect(subject.put("/test", key: "value").body).to eq("key=value")
        end
      end
    end
  end
end
