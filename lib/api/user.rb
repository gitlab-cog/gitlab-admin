require_relative "client"

module GitLab
  module API
    User = Struct.new(:id, :username, :name, :email, :state, :created_at,
                      :two_factor_enabled, :last_sign_in_at, :current_sign_in_at,
                      :last_activity_on)

    Event = Struct.new(:created_at, :action_name, :target_type, :target_title)

    class UserFinder
      def initialize(username, client)
        fail Cog::Error.new("A username is required") if username.nil? || username.empty?
        @username = username
        @client = client
      end

      def find
        response = if @username.include? "@"
                     @client.get("/api/v4/users?search=#{CGI.escape(@username)}")
                   else
                     @client.get("/api/v4/users?username=#{CGI.escape(@username)}")
                   end

        user = case response.code.to_i
               when 200
                 JSON.parse(response.body).shift
               when 404
                 nil
               else
                 fail Cog::Error.new("Error while trying to find user #{@username}: " \
                                     "#{response.code} - #{response.message}")
               end
        build(user)
      end

      private

      def build(payload)
        fail Cog::Error.new("User #{@username} does not exists") if payload.nil?
        User.from_hash(payload)
      end
    end

    class UserClient
      def initialize(client = Client.new)
        @client = client
      end

      def find(username)
        user = UserFinder.new(username, @client).find
        if block_given?
          yield user
        else
          user
        end
      end

      def block(username)
        find(username) do |user|
          response = @client.post("/api/v4/users/#{user.id}/block")
          unless response.code == "201"
            fail Cog::Error.new("Cannot block user #{username}: #{response.code} - " \
                                "#{response.body}")
          end
        end
      end

      def unblock(username)
        find(username) do |user|
          response = @client.post("/api/v4/users/#{user.id}/unblock")
          unless response.code == "201"
            fail Cog::Error.new("Cannot unblock user #{username}: #{response.code} - " \
                                "#{response.body}")
          end
        end
      end

      def update_email(username, new_email)
        find(username) do |user|
          response = @client.put("/api/v4/users/#{user.id}?email=#{new_email}")
          unless response.code == "200"
            fail Cog::Error.new("Cannot change user #{username}'s email to #{new_email}: " \
                                "#{response.code} - #{response.body}")
          end
        end
      end
    end

    class Events
      def initialize(client = Client.new)
        @client = client
      end

      def last(username)
        list(username, 1).shift
      end

      def list(username, limit = 5)
        user = UserFinder.new(username, @client).find
        response = @client.get("/api/v4/users/#{user.id}/events?per_page=#{limit}")
        case response.code.to_i
        when 200
          JSON.parse(response.body).map { |event| Event.from_hash(event) }
        when 404
          []
        else
          fail Cog::Error.new("Failed to get Events for user #{username}: #{response.code} - #{response.message}")
        end
      end
    end
  end
end
