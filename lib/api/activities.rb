require_relative "client"

module GitLab
  module API
    class ActivitiesClient
      def initialize(client = Client.new)
        @client = client
      end

      def get(from)
        fail "Date from is required" if from.nil? || from.empty?
        response = @client.get("api/v4/user/activities?from=#{from}")
        case response.code.to_i
        when 200
          response.header["X-Total"] + " total active users from #{from}"
        else
          fail "Failed to get active users from #{from}: #{response.code} - #{response.body}"
        end
      end
    end
  end
end
