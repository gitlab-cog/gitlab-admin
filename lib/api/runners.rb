require_relative "client"

module GitLab
  module API
    class Runners
      def initialize(runners)
        @runners = runners
      end

      def to_a
        @runners.map(&:to_h)
      end
    end

    Runner = Struct.new(:id, :description, :active, :is_shared, :contacted_at, :version, :tag_list)

    class RunnersClient
      def initialize(client = Client.new)
        @client = client
      end

      def list
        response = @client.get("/api/v4/runners/all?scope=shared")
        case response.code.to_i
        when 200
          runners = JSON.parse(response.body).map { |runner| Runner.from_hash(runner) }
          Runners.new(runners)
        else
          fail Cog::Error.new("Failed to list runners: #{response.code} - #{response.message}")
        end
      end

      def info(id)
        id_required!(id)
        response = @client.get("/api/v4/runners/#{CGI.escape(id)}")
        handle_response("show details of Runner #{id}", response)
      end

      def pause(id)
        id_required!(id)
        response = @client.put("/api/v4/runners/#{CGI.escape(id)}", active: false)
        handle_response("pause Runner #{id}", response)
      end

      def unpause(id)
        id_required!(id)
        response = @client.put("/api/v4/runners/#{CGI.escape(id)}", active: true)
        handle_response("unpause Runner #{id}", response)
      end

      private

      def handle_response(name, response)
        case response.code.to_i
        when 200
          runner = JSON.parse(response.body)
          Runner.from_hash(runner)
        else
          fail Cog::Error.new("Failed to #{name}: #{response.code} - #{response.message}")
        end
      end

      def id_required!(id)
        fail Cog::Error.new("Runner's ID is required") if id.nil? || id.empty? || id.to_i < 1
      end
    end
  end
end
