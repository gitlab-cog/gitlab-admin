require_relative "client"

module GitLab
  module API
    BroadcastMessage = Struct.new(:message, :starts_at, :ends_at, :id, :active) do
      def self.from_hash(bcast)
        new(*bcast.values_at(*members.map(&:to_s)))
      end

      def to_s
        "'#{message}' from '#{starts_at}' to '#{ends_at}' with id #{id} currently " +
          (active ? "active" : "inactive")
      end
    end

    class BroadcastClient
      def initialize(client = Client.new)
        @client = client
      end

      def get
        response = @client.get("/api/v4/broadcast_messages")
        fail Cog::Error.new("Failed to get broadcast messages: #{response.code} - #{response.message}") unless
          response.code == "200"
        JSON.parse(response.body).map { |bcast| BroadcastMessage.from_hash(bcast) }
      end

      def add(message, starts_at, ends_at)
        args             = { message: message }
        args[:starts_at] = starts_at unless starts_at.nil?
        args[:ends_at]   = ends_at unless ends_at.nil?

        response = @client.post("/api/v4/broadcast_messages", args)
        fail Cog::Error.new("Failed to set broadcast message: #{response.code} - #{response.message}") unless
          %w[200 201].include?(response.code)
        "Broadcast message set to #{BroadcastMessage.from_hash(JSON.parse(response.body))}"
      end
    end
  end
end
