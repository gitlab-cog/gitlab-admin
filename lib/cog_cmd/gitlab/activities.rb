require_relative "helper"
require "api/activities"
require "date"

module CogCmd
  module Gitlab
    class Activities < Cog::Command
      def run_command
        from = request.args[0] || Date.today.to_s
        response.content = GitLab::API::ActivitiesClient.new.get(from).to_s
      end
    end
  end
end
