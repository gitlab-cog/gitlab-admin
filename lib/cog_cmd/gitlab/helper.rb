require "cog"

module Args
  def self.required(value, message)
    fail Cog::Error.new(message) if value.nil? || value.empty?
    value
  end
end
