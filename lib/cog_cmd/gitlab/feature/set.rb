require_relative "../helper"
require "api/feature"

module CogCmd
  module Gitlab
    module Feature
      class Set < Cog::Command
        def run_command
          name = Args.required(request.args[0], "I need a feature name to set gate values")
          value = Args.required(request.args[1], "I need a gate value to update the feature")
          feature = GitLab::API::FeatureClient.new.set(name, value)
          response.template = "feature_list"
          response.content = feature.to_a
        end
      end
    end
  end
end
