require_relative "../helper"
require "api/user"

module CogCmd
  module Gitlab
    module User
      class Unblock < Cog::Command
        def run_command
          username = Args.required(request.args[0], "Username is required to unblock")
          GitLab::API::UserClient.new.unblock(username)
          response.content = "User #{username} has been unblocked"
        end
      end
    end
  end
end
