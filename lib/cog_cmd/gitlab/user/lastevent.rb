require_relative "../helper"
require "api/user"

module CogCmd
  module Gitlab
    module User
      class Lastevent < Cog::Command
        def run_command
          username = Args.required(request.args[0], "Username is required to perform a search")
          last_event = GitLab::API::Events.new.last(username)
          response.template = "user_events"
          response.content = last_event.to_a
        end
      end
    end
  end
end
