require_relative "../../helper"
require "api/user"

module CogCmd
  module Gitlab
    module User
      module Email
        class Update < Cog::Command
          def run_command
            username = Args.required(request.args[0], "Username is required to update email")
            new_email = Args.required(request.args[1], "New email is required to update email")
            GitLab::API::UserClient.new.update_email(username, new_email)
            response.content = "User #{username} was sent a confirmation email to #{new_email}"
          end
        end
      end
    end
  end
end
