require_relative "../helper"
require "api/user"

module CogCmd
  module Gitlab
    module User
      class Find < Cog::Command
        def run_command
          username = Args.required(request.args[0], "Username or email is required to perform a search")
          user = GitLab::API::UserClient.new.find(username)
          response.template = "user_find"
          response.content = [user.to_h]
        end
      end
    end
  end
end
