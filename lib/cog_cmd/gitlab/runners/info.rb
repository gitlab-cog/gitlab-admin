require_relative "../helper"
require "api/runners"

module CogCmd
  module Gitlab
    module Runners
      class Info < Cog::Command
        def run_command
          runner_id = Args.required(request.args[0], "I need a Runner ID to get Runner's details")
          runner = GitLab::API::RunnersClient.new.info(runner_id)
          response.template = "runners_info"
          response.content = runner.to_h
        end
      end
    end
  end
end
